public class acfMobileVerification {
	
    public String errMsg;

    public acfMobileVerification() {
        errMsg = '';
    }
   
	// This method is used to verify OTP 
    /*public void verifyPin(String idContact, String insertedPIN, String mobileNo) {
      errMsg = '';
      try {
      	contact objcon = [select id, MobilePhone,Is_Mobile_No_Valid__c, PIN__c, PIN_Created__c from contact where id =: idContact];
        if((system.now().getTime() - objcon.PIN_Created__c.getTime()) < 1800000) {
                if(objcon.PIN__c == insertedPIN) {
                    objcon.MobilePhone = mobileNo;
                    objcon.Is_Mobile_No_Valid__c = true;
                    update objcon ;
                }  else   {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter The Valid Pin');
                    ApexPages.addMessage(msg);
                }
            }   else  {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Your PIN has expired. Please regenerate!');
                ApexPages.addMessage(msg);
            }
        } catch(Exception e)   {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error found. Please try again!');
            ApexPages.addMessage(msg);
        }
    }*/
    
    // This method is used to generate OTP and send to mobile number as text message.
    public void sendMessage(string mobileNo, String idContact)  {
        errMsg = '';
        
        if(mobileNo != null && mobileNo.length() == 10)   {
            try  {
                // SMS_Password__c obSMS = SMS_Password__c.getOrgDefaults();
                String PIN = String.valueOf(system.now().getTime()).substring(String.valueOf(system.now().getTime()).length() - 6);
                /*
                String tmpStr = '<?xml version="1.0" encoding="UTF-8" ?> <MESSAGE><USER USERNAME="'+obSMS.Name+'" PASSWORD="'+obSMS.Password__c+'"/><SMS UDH="0" CODING="1" TEXT="Your OTP PIN is: '+PIN+'" PROPERTY="0" ID="'+PIN+'_'+idLead+'"><ADDRESS FROM="'+obSMS.Sender_Id__c+'" TO="'+mobileNo+'" SEQ="1" TAG="some clientside random data" /></SMS></MESSAGE>';
                HttpRequest req = new HttpRequest();
                //String str = 'http://api.myvaluefirst.com/psms/servlet/psms.Eservice2?data='+EncodingUtil.urlEncode(tmpStr, 'UTF-8')+'&action=send';
                //req.setEndpoint(str);
                req.setMethod('GET');
                req.setTimeout(60000);
                Http http = new Http();
                HttpResponse res = new HttpResponse();
                res = http.send(req);
                */
                /*contact objcon = [select id, PIN__c, PIN_Created__c from contact where id=:idContact];
                objcon.PIN__c = PIN;
                objcon.PIN_Created__c = system.now();
                update objcon;              */
            } catch(Exception e) { }
         }  else  {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Provide a valid Mobile Number.');
            ApexPages.addMessage(msg);
        }
    }
    
   // This method is used to apply validation on mobile number
    public boolean IsValidMobileNumber(string mobileNo) {
        if(mobileNo.length() == 10)
            return true;
        else
            return false;                    
    }  
}