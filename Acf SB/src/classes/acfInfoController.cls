Public without sharing class acfInfoController{
    public acfContentManagementSystem__c cms_obj {get; set;}
    public list<acfContentManagementSystem__c> lst_sctn_features{get; set;}
    Public String pgName {get;set;}
    Public acfInfoController(){
        Cookie Bname = new Cookie('Bname','',null,-1,true); 
        ApexPages.currentPage().setCookies( new Cookie[]{Bname});
        String strPageId = apexpages.currentpage().getparameters().get('pid');
        If(strPageId != null && strPageId <> ''){
            acfCMS_Page__c ObjCMSPage = [select id,Name from acfCMS_Page__c where id=:strPageId];
            pgName = ObjCMSPage.Name;
            cms_obj = [select id,acfDescription__c,acfHeadingRemoveSpace__c, acfHeading__c, acfImageURL__c, acfMoreDetailURL__c, acfSectionName__c, acfSequence__c,acfDescriptionImageURL__c,Name,acfCMS_Page__c,acfPageName__c  from acfContentManagementSystem__c where acfPageName__c = 'common' and acfSectionName__c = 'Footer' limit 1];
            lst_sctn_features = [SELECT acfDescription__c,acfHeadingRemoveSpace__c, acfHeading__c, acfImageURL__c, acfMoreDetailURL__c, acfSectionName__c, acfSequence__c,acfDescriptionImageURL__c, Id,acfCMS_Page__c, Name FROM acfContentManagementSystem__c WHERE acfCMS_Page__c =:strPageId AND acfSectionName__c != 'Footer Links' AND acfIs_Checked__c = true order by acfSequence__c]; 
        }  
    }
    
    public pagereference getStartedLast()
    {
        Cookie Bname = new Cookie('Bname','',null,-1,true); 
        ApexPages.currentPage().setCookies( new Cookie[]{Bname});
        Cookie currentUId = ApexPages.currentPage().getCookies().get('Bname');
        system.debug('@@@@@'+currentUId);
        pagereference pageref = new pagereference('/apex/acfPreloginQuestions');
        pageref.setredirect(true);
        return pageref;
    }
}