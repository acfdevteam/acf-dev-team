@isTest(SeeAllData = false)
private class acfId_VerifyControllerTracker {
	private static User objUser;
	private static Contact objContact;
	private static Account objAccount;
	private static Opportunity objOpportunity;
	
    static testMethod void validateacfId_VerifyController() 
    {
		LoadData();
		acfId_VerifyController objacfId_VerifyController = new acfId_VerifyController();
		objacfId_VerifyController.redirect();
		objacfId_VerifyController.redirectToJumio();
		objacfId_VerifyController.redirectToNextPage();

    }
    
    static testMethod void validateacfId_VerifyController1() 
    {
		createAccount();
    	createContact();
    	createUser();
		acfId_VerifyController objacfId_VerifyController = new acfId_VerifyController();
		objacfId_VerifyController.redirect();
		objacfId_VerifyController.redirectToJumio();
		objacfId_VerifyController.redirectToNextPage();

    }
    private static void LoadData()
    {
    	createAccount();
    	createContact();
    	createUser();
    	createOpportunity();
    }
    
    static void createAccount()
    {
    	objAccount = new Account();
    	objAccount = acfCommontrackerClass.createAccount(objAccount);
    }
    
    static void createContact()
    {
    	objContact = new Contact();
    	//objContact.AccountId = objAccount.id;
    	//objContact.acf_lead__c = objLead.id;
    	objContact = acfCommontrackerClass.createContact(objContact,objAccount);
    }
    
    static void createuser()
    {
    	objUser = new User();
    	objuser.ContactId = objContact.id;
    	objUser = acfCommontrackerClass.createuser(objUser,objContact,objAccount);
    }
	
	 static void createOpportunity()
    {
      objOpportunity = new Opportunity();
      objOpportunity.AccountId = objuser.AccountId;
      objOpportunity.name = 'test';
      objOpportunity.Stagename = 'Application Taken';
      objOpportunity.CloseDate = System.today();
      insert objOpportunity;
    }
    
}