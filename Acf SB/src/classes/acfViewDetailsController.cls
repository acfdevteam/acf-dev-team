public without sharing class acfViewDetailsController 
{
    public String strOppId;
    public wrapperOppDetails objwrapperOppDetails {get;set;}
    public acfViewDetailsController()
    {
       objwrapperOppDetails = new wrapperOppDetails();
       strOppId = acfCommon.getCurrentLoggedInOpportunityId(userInfo.getuserId()); 
       system.debug('@@@@oppId'+strOppId);
       If(strOppId != null && strOppId <> '')
       {
          List<Opportunity> List_Opp = [select id ,Current_Lender__c ,Transaction_Type__c,Amount,Current_Interest_Rate__c,acfComparison_Rate__c,acfAddRepayments__c,acfInterestOnly__c,acfCurrentLoanAmount__c,
                                        acf_Redraw__c,acfMultipleSplits__c,Car_Application_Fee__c,acfOngoing_Fees__c,acfValuation_Fee__c,acfLegal_Fees__c,acfProduct_LVR__r.acfProduct__r.Description,acfProduct_LVR__r.acfProduct__r.Name,acfDebitCard__c,acfOffsetAccount__c,
                                        (select id,pricebookEntry.Product2.acfRating1__c from opportunitylineitems limit 1)
                                        from opportunity where id=:strOppId];
          system.debug('@@@@karthik_List'+List_Opp);
          If(List_Opp != null && List_Opp.size() > 0)
          {
              objwrapperOppDetails.oppcurrentLender = List_Opp[0].acfProduct_LVR__r.acfProduct__r.Name;
              objwrapperOppDetails.strProdDescription = List_Opp[0].acfProduct_LVR__r.acfProduct__r.Description;
              objwrapperOppDetails.oppAmount =  '$'+string.valueof(List_Opp[0].acfCurrentLoanAmount__c);
              objwrapperOppDetails.oppInterestRate = string.valueof(List_Opp[0].Current_Interest_Rate__c)+'%';
              objwrapperOppDetails.oppCompRate = string.valueof(List_Opp[0].acfComparison_Rate__c)+'%';
              objwrapperOppDetails.oppaddrepay = List_Opp[0].acfAddRepayments__c;
              objwrapperOppDetails.oppInterest = List_Opp[0].acfInterestOnly__c;
              objwrapperOppDetails.oppDebitcard = List_Opp[0].acfDebitCard__c;
              objwrapperOppDetails.oppOffsetAcc = List_Opp[0].acfOffsetAccount__c;
              objwrapperOppDetails.oppRedraw = List_Opp[0].acf_Redraw__c;
              objwrapperOppDetails.oppMultiplesplits = List_Opp[0].acfMultipleSplits__c;
              objwrapperOppDetails.oppAppfee = '$'+string.valueof(List_Opp[0].Car_Application_Fee__c);
              objwrapperOppDetails.oppOngoingfee = '$'+string.valueof(List_Opp[0].acfOngoing_Fees__c);
              objwrapperOppDetails.oppValuationfee = '$'+string.valueof(List_Opp[0].acfValuation_Fee__c);
              objwrapperOppDetails.oppLegalfee = '$'+string.valueof(List_Opp[0].acfLegal_Fees__c); 
              If(objwrapperOppDetails.oppInterestRate.contains('null')){
                  objwrapperOppDetails.oppInterestRate = '0.00%';
                  system.debug('@@@@@var'+objwrapperOppDetails.oppInterestRate);
              }  
              If(objwrapperOppDetails.oppCompRate.contains('null')){
                  objwrapperOppDetails.oppCompRate = '0.00%';
              } 
              If(objwrapperOppDetails.oppAmount.contains('null')){
                  objwrapperOppDetails.oppAmount = '$0';
              }  
              If(objwrapperOppDetails.oppAppfee.contains('null')){
                  objwrapperOppDetails.oppAppfee = '$0';
              } 
              If(objwrapperOppDetails.oppOngoingfee.contains('null')){
                  objwrapperOppDetails.oppOngoingfee = '$0';
              }
              If(objwrapperOppDetails.oppValuationfee.contains('null')){
                  objwrapperOppDetails.oppValuationfee = '$0';
              }
              If(objwrapperOppDetails.oppLegalfee.contains('null')){
                  objwrapperOppDetails.oppLegalfee = '$0';
              }
              //added by lokesh to show rating.
              
              if(List_Opp[0].opportunitylineitems <> null && List_Opp[0].opportunitylineitems.size()>0)
              {
              	if(List_Opp[0].opportunitylineitems[0].pricebookEntry.Product2.acfRating1__c <> null)
              	{
              		objwrapperOppDetails.lstProductRating = new List<Integer>();
              		for(integer i=1;i<=Integer.valueof(List_Opp[0].opportunitylineitems[0].pricebookEntry.Product2.acfRating1__c);i++)
                    {
                          objwrapperOppDetails.lstProductRating.add(i);
                    }
                    string Rating = string.valueof(List_Opp[0].opportunitylineitems[0].pricebookEntry.Product2.acfRating1__c);
                    system.debug('!@#$%'+Rating);
                    Rating = Rating.substring(2,4);
                    system.debug('#$%'+Rating);
                    objwrapperOppDetails.strRating = string.valueof(List_Opp[0].opportunitylineitems[0].pricebookEntry.Product2.acfRating1__c)+'/5';
                    if(Rating <> null && Integer.valueof(Rating)>0)
                    {
                    	objwrapperOppDetails.IsDisplayHalfStar = true;
                    }
              	}
              }
          }           
       }
    }
    
    Public pagereference redirect()
    {
        String oppId = acfCommon.getCurrentLoggedInOpportunityId(userInfo.getuserId()); 
        If(oppId != null && oppId <> ''){
           return null;
        }else{                
                String retUrl  = acfCommon.sendReturnUrl(userInfo.getuserId(),false,'acfViewDetails'); 
                Pagereference pageref= new Pagereference('/'+retUrl);
                return pageref;
             }
    }
    
    Public class wrapperOppDetails {
        public String oppcurrentLender {get;set;}
        public string strProdDescription {get;set;}
        public String oppAmount {get;set;}
        public String oppInterestRate {get;set;}
        public String oppCompRate {get;set;}
        public Boolean oppaddrepay {get;set;}
        public Boolean oppInterest {get;set;}
        public Boolean oppDebitcard {get;set;}
        public Boolean oppOffsetAcc {get;set;}
        public Boolean oppRedraw {get;set;}
        public Boolean oppMultiplesplits {get;set;}
        public String oppAppfee {get;set;}
        public String oppOngoingfee {get;set;}
        public String oppValuationfee {get;set;}
        public String oppLegalfee {get;set;}
        public List<Integer>lstProductRating{get;set;}
        public boolean IsDisplayHalfStar{get;set;}
        public string strRating{get;set;}
   }
}