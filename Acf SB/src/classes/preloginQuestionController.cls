public class preloginQuestionController 
{
    //public variables declaration
    public map<decimal,Id>mapQuestionSequenceNo;
    public map<id,Question__c>mapQuestionIdToQuestion;
    public map<id,Answer__c>mapAnswerIdToAnswer;
    public integer indexNo{get;set;}
    public integer QuesNo{get;set;}
    public List<WrapperQuestions>lstMasterWrapperQuestion{get;set;}
    public List<WrapperQuestions>lstWrapperQuestion{get;set;}
    //Constructor 
    public preloginQuestionController ()
    {
      mapQuestionSequenceNo    = new map<decimal,Id>();
      lstMasterWrapperQuestion = new List<WrapperQuestions>();
      lstWrapperQuestion       = new List<WrapperQuestions>();
      mapAnswerIdToAnswer      = new map<id,Answer__c>();
      mapQuestionIdToQuestion  = new map<id,Question__c>();
      Id ScriptId;
      indexNo = 0;
      List<Script__c>lstScript = [select id,acf_Sequence_no__c,acf_Type__c from Script__c where acf_Type__c=:'Pre-Login' order by acf_Sequence_no__c];
      if(lstScript <> null && lstScript.size()>0)
      {
         ScriptId = lstScript[0].id;
      }
      if(ScriptId <> null)
      {
        for(Question__c objQuestion:[select id,acf_ApiName__c,acf_Description__c,acf_Master_Question__c,acf_Question__c,acf_Script__c,acf_Type__c,
                                       (select id,acf_Answer__c,acf_Question__c,acf_Related_Question__c from Answer__r) from Question__c
                                       where (acf_Master_Question__r.acf_Script__c =: ScriptId or acf_Script__c =:ScriptId) order by acf_Level__c,acf_Sequence_No__c])  
         {                                      
             mapQuestionIdToQuestion.put(objQuestion.id,objQuestion);
             WrapperQuestions objWarpperQues = new WrapperQuestions();
             objWarpperQues.objWrapQuestion = objQuestion;
             if(objQuestion.acf_Script__c <> null)
             {
               lstMasterWrapperQuestion.add(objWarpperQues);
             }
         }
         //for adding wrapperlist in a page.  
         for(WrapperQuestions objMasterQuestion:lstMasterWrapperQuestion)
         {
            objMasterQuestion.lstSelectOpt = new List<SelectOption>();
            for(Answer__c objAnswer:objMasterQuestion.objWrapQuestion.Answer__r)
            {
                objMasterQuestion.lstSelectOpt.add(new SelectOption((objAnswer.id),objAnswer.acf_Answer__c));
            }
            lstWrapperQuestion.add(objMasterQuestion); 
         }                                                     
      }                            
    }
    public WrapperQuestions NextMasterQuestion(Integer indexNum)
    {
        //for adding wrapper-list in a page.
        List<WrapperQuestions>lstWrapperQuestion = new List<WrapperQuestions>();
        if(indexNum < lstMasterWrapperQuestion.size() && lstMasterWrapperQuestion[indexNum]<> NUll)
        {
            WrapperQuestions objMasterQuestion = lstMasterWrapperQuestion[indexNum];
            objMasterQuestion.lstSelectOpt = new List<SelectOption>();
            for(Answer__c objAnswer:objMasterQuestion.objWrapQuestion.Answer__r)
            {
                objMasterQuestion.lstSelectOpt.add(new SelectOption((objAnswer.id),objAnswer.acf_Answer__c));
            }
            return objMasterQuestion;
        } 
        return null;   
    }
    public class WrapperQuestions
    {
      public Question__c objWrapQuestion{get;set;}
      public string strWrapAnswer{get;set;}
      public Answer__c objWrapAnswer{get;set;}
      public List<SelectOption>lstSelectOpt{get;set;}
      public List<WrapperQuestions>lstRelatedQuestion{get;set;}
    }
    
    public pageReference RelatedQuestions()
    {
        Id RelatedQuesId ;
        if(lstWrapperQuestion.size()>indexNo)
        {   
            WrapperQuestions objQuestion = lstWrapperQuestion[indexNo];
            objQuestion.lstRelatedQuestion = new List<WrapperQuestions>();
            for(Answer__c objAnswer:objQuestion.objWrapQuestion.Answer__r)
            {
                if(objQuestion.strWrapAnswer <> null && objQuestion.strWrapAnswer <> 'NONE')
                {  
                    if(objAnswer.id==objQuestion.strWrapAnswer)
                    {
                        if(objAnswer.acf_Related_Question__c <> null)
                        {
                             RelatedQuesId = objAnswer.acf_Related_Question__c;
                             break;
                        }
                    }
                }            
            }
            if(RelatedQuesId <> null && mapQuestionIdToQuestion.get(RelatedQuesId) <> null)
            {
                WrapperQuestions objRelatedQuestion = new WrapperQuestions();
                objRelatedQuestion.objWrapQuestion = mapQuestionIdToQuestion.get(RelatedQuesId);
                objRelatedQuestion.lstSelectOpt = new List<SelectOption>();
                for(Answer__c objAnswer:objRelatedQuestion.objWrapQuestion.Answer__r)
                {
                    objRelatedQuestion.lstSelectOpt.add(new SelectOption((objAnswer.id),objAnswer.acf_Answer__c));
                }
                objQuestion.lstRelatedQuestion.add(objRelatedQuestion);
            }            
        }
         return null;
    }
    public pageReference AddingRelatedQuestion()
    {
        system.debug('@@@@@LOkesh'+QuesNo);
        Id RelatedQuesId;
        set<Integer>setIndexToRemove = new set<Integer>();
        if(lstWrapperQuestion.size()>indexNo)
        {   
            WrapperQuestions objQuestion = lstWrapperQuestion[indexNo];
            if(objQuestion <> null && objQuestion.lstRelatedQuestion.size()>0)
            {
                //If any answer is changed then remove all child nodes from the list related to that answer.
                Integer tempQuesNo = QuesNo+1;
                Integer RelatedQustionWrapperSize = objQuestion.lstRelatedQuestion.size();          
                while(tempQuesno < RelatedQustionWrapperSize)
                {
                  setIndexToRemove.add(tempQuesno); 
                  tempQuesno++;
                }
                system.debug('@@@@::'+setIndexToRemove);
                if(setIndexToRemove.size()>0)
                {
                    boolean IsFirstIndex = true;
                    for(Integer index:setIndexToRemove)
                    {
                      system.debug('@@@@@:::'+index);
                      if(IsFirstIndex == false)
                      {
                         --index;
                      }
                      if(objQuestion.lstRelatedQuestion.size()>index)
                      objQuestion.lstRelatedQuestion.remove(index);
                      IsFirstIndex = false; 
                    }
                }               
                if(objQuestion.lstRelatedQuestion.size()>QuesNo && objQuestion.lstRelatedQuestion[QuesNo] <> null)
                {
                     WrapperQuestions objRelatedQuestion = objQuestion.lstRelatedQuestion[QuesNo];
                     system.debug('::::@@@###'+objRelatedQuestion);
                     for(Answer__c objAnswer:objRelatedQuestion.objWrapQuestion.Answer__r)
                     {
                        if(objRelatedQuestion.strWrapAnswer <> null && objRelatedQuestion.strWrapAnswer <> 'NONE')
                        {  
                            if(objAnswer.id==objRelatedQuestion.strWrapAnswer)
                            {
                                if(objAnswer.acf_Related_Question__c <> null)
                                {
                                     RelatedQuesId = objAnswer.acf_Related_Question__c;
                                     break;
                                }
                            }
                        }            
                     }
                     if(RelatedQuesId <> null && mapQuestionIdToQuestion.get(RelatedQuesId) <> null)
                     {
                        WrapperQuestions objRelatedQuestions = new WrapperQuestions();
                        objRelatedQuestions.objWrapQuestion = mapQuestionIdToQuestion.get(RelatedQuesId);
                        objRelatedQuestions.lstSelectOpt = new List<SelectOption>();
                        for(Answer__c objAnswer:objRelatedQuestions .objWrapQuestion.Answer__r)
                        {
                            objRelatedQuestions.lstSelectOpt.add(new SelectOption((objAnswer.id),objAnswer.acf_Answer__c));
                        }
                        objQuestion.lstRelatedQuestion.add(objRelatedQuestions);
                     } 
               }
            }
        }    
        return null;
    }
    public void NextQuestion()
    {
      if(lstWrapperQuestion.size()>indexNo)
       ++indexNo;
    }
}