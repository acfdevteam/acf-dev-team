<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>About_Conga_Composer</defaultLandingTab>
    <description>Conga Composer makes it easy for salesforce.com customers to create sophisticated documents, presentations and reports using data from any standard or custom object.</description>
    <label>Conga Composer</label>
    <tab>About_Conga_Composer</tab>
    <tab>Conga_Setup</tab>
    <tab>Conga_Template__c</tab>
    <tab>Conga_Email_Template__c</tab>
    <tab>Conga_Merge_Query__c</tab>
    <tab>CongaMerge</tab>
    <tab>standard-report</tab>
    <tab>acfBank_Detail__c</tab>
    <tab>acfContentManagementSystem__c</tab>
    <tab>Question__c</tab>
    <tab>Script__c</tab>
    <tab>acfTask_Master__c</tab>
    <tab>Dependent_Document__c</tab>
    <tab>Document_Master__c</tab>
    <tab>Required_Document__c</tab>
    <tab>acfDependent_Product__c</tab>
    <tab>acfSuggested_Product__c</tab>
    <tab>acfCMS_Page__c</tab>
</CustomApplication>
